# We Collect No Personal Information Using Our Applications

We do not collect, use, save or have access to any of your personal data recorded in HideTheWord for iOS, macOS or tvOS.

Individual settings relating to the HideTheWord apps are not personal and are stored only on your device. You might also be asked to provide access to your photo library, but this is only so you can open your photos in HideTheWord and save one as your student profile photo. We don't process that information at all and have no access to it.